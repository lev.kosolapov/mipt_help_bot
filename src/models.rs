use chrono::NaiveDateTime;
use diesel::*;
use schema::*;


#[derive(Debug, Insertable, Queryable)]
#[table_name = "scholars"]
pub struct Scholar<'a> {
    pub name: &'a str,
    pub faculty: i16,
    pub group: i16,
    pub telegram_id: i64,
    pub telegram_name: &'a str,
}

#[derive(Debug, Insertable, Queryable)]
#[table_name = "lessons"]
pub struct Lesson {

    pub repet_id: Option<i32>,
    pub scholar_id: Option<i32>,
    pub date: Option<NaiveDateTime>,
    pub length: Option<f32>,
    pub subject: Option<i16>,
    pub stage: Option<i16>,
    pub report_id: Option<i32>,
    pub id: i32,
}


#[derive(Debug, Insertable, Queryable, QueryableByName)]
#[table_name = "users"]
pub struct User {
    pub id: i32,
    pub telegram_id: i64,
    pub groups: i16,
}

#[derive(Debug, Insertable, Queryable)]
#[table_name = "reports"]
pub struct Report {

    pub scholars_report: Option<String>,
    pub tutors_report: Option<String>,
    pub prooflink_photo: Option<String>,
    pub rating: Option<f32>,
    pub tutor_id: i32,
    pub scholar_adeq: Option<f32>,
    pub id: i32,
}

