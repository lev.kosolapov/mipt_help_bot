#![feature(extern_prelude)]
extern crate futures;
extern crate serde;
extern crate serde_json;
extern crate telegram_bot;
extern crate telegram_bot_raw;
extern crate tokio_core;
#[macro_use]
extern crate serde_derive;
#[macro_use]
#[macro_use(slog_o, slog_kv)]
extern crate slog;
extern crate slog_async;
extern crate slog_scope;
extern crate slog_term;
#[macro_use]
extern crate diesel;
extern crate chashmap;
extern crate chrono;
extern crate dotenv;
extern crate regex;

mod command;
mod command_implementation;
mod command_tools;
mod concurrency_manager;
mod db;
mod file_tools;
mod models;
mod schema;
mod sender;

use command::{CommandError, CommandList};
use command_implementation::{add_commands, catch_callback_query};
use concurrency_manager::Manager;
use dotenv::dotenv;
use futures::Stream;
use slog::Drain;
use std::env;
use std::fs::OpenOptions;
use std::sync::Arc;
use telegram_bot::*;
use tokio_core::reactor::Core;

fn start_logging() -> slog_scope::GlobalLoggerGuard {
    let log_path = "log_file";
    let file = OpenOptions::new()
        .create(true)
        .write(true)
        .append(true)
        .open(log_path)
        .expect("can't open file");
    //.unwrap();

    let decorator = slog_term::PlainDecorator::new(file);
    let drain = slog_term::FullFormat::new(decorator).build().fuse();
    let drain = slog_async::Async::new(drain).build().fuse();
    let logger = slog::Logger::root(drain, slog_o!());

    let _scope_guard = slog_scope::set_global_logger(logger);

    slog_info!(slog_scope::logger(), "logging redirected to file");

    _scope_guard
}

fn main() {
    dotenv().ok();
    match file_tools::load_data_to_tmp() {
        Ok(_) => (),
        Err(err) => panic!("{}", err),
    }
    db::establish_connection();
    let _scope_guard = start_logging();
    slog_info!(slog_scope::logger(), "start bot");

    let token = env::var("BOT_TOKEN").expect("BOT_TOKEN must be set");

    let mut core = Core::new().unwrap();
    let handle = core.handle();
    let api = Api::configure(&token).build(&handle).unwrap();

    let manager = Arc::new(Manager::new());
    let command_list = CommandList::new(&manager, add_commands, catch_callback_query);

    // Fetch new updates via long poll method
    slog_info!(slog_scope::logger(), "listening started");
    let future = api.stream().for_each(|update| {
        slog_info!( slog_scope::logger(), "";
                    "update received" => format!("{:?}",&update));

        // If false was returned, then bot does not process the command,
        // maybe the user sent a command right now
        let update = match manager.try_send(update) {
            Some(value) => value,
            None => return Ok(()),
        };

        let update = match command_list.try_catch_callback_query(update) {
            Some(value) => value,
            None => return Ok(()),
        };

        // If the received update contains a new message...
        if let UpdateKind::Message(message) = &update.kind {
            if let MessageKind::Text { ref data, .. } = message.kind {
                let user_id = message.from.to_user_id().clone();
                // Print received text message to stdout.
                println!("<{}>:{}", user_id, data);

                // Обработка запросов
                let update = update.clone();
                let result = command_list.try_run_command(data, update, user_id.clone());
                match result {
                    Ok(_) => (),
                    Err(error) => {
                        slog_info!(slog_scope::logger(), "Command error";
                                            "user" => format!("{}", user_id));
                        let report = match error {
                            CommandError::NotACommand => {
                                "Пожалуйста, введите команду"
                            }
                            CommandError::InvalidCommand => "Неверная команда",
                            CommandError::InvalidArgs => {
                                "Неверные аргументы команды"
                            }
                            CommandError::NoAccess => {
                                "Нет доступа к этой функции"
                            }
                        };
                        let chat: ChatId = user_id.into();
                        api.spawn(chat.text(format!("{}", report)));
                    }
                }
            }
        }

        Ok(())
    });

    core.run(future).unwrap();
}
