use std::fs::{create_dir, File, OpenOptions};
use std::io::{Error, Read, Write};

pub fn _read_file(path: &str) -> Result<String, Error> {
    let file = File::open(path);
    match file {
        Ok(mut file) => {
            let mut content = String::new();
            file.read_to_string(&mut content)?;
            Ok(content)
        }
        Err(err) => Err(err),
    }
}

pub fn load_file_from_tmp(filename: &str) -> Result<String, Error> {
    let file = File::open("/tmp/mipt_help_bot/".to_owned() + filename);
    match file {
        Ok(mut file) => {
            let mut content = String::new();
            file.read_to_string(&mut content)?;
            Ok(content)
        }
        Err(err) => Err(err),
    }
}

fn upload_file(buffer: &mut Vec<u8>, name: &str) -> Result<(), Error> {
    let themes_file = File::open("../../data/".to_owned() + name);
    let mut themes_file = match themes_file {
        Ok(value) => value,
        Err(_) => File::open("./data/".to_owned() + name)?,
    };
    match themes_file.read_to_end(buffer) {
        Ok(_) => Ok(()),
        Err(err) => Err(err),
    }
}

fn load_file(buffer: &mut Vec<u8>, name: &str) -> Result<(), Error> {
    let mut tmp_file = OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(true)
        .open("/tmp/mipt_help_bot/".to_owned() + name)?;
    tmp_file.write_all(&buffer)
}

fn copy_to_tmp(name: &str) -> Result<(), Error> {
    let mut buffer: Vec<u8> = Vec::new();
    upload_file(&mut buffer, name)?;
    load_file(&mut buffer, name)
}

pub fn load_data_to_tmp() -> Result<(), Error> {
    create_dir("/tmp/mipt_help_bot");
    copy_to_tmp("Sections.json")?;
    copy_to_tmp("Subjects.json")?;

    Ok(())
}
